﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework.ThirdParty
{
    public interface IAccount
    {
		public int Id { get; }
		public bool IsRegistered { get; }
		public bool IsConfirmed { get; }
		public int ActionsSuccessfullyPerformed { get; }

		public void Register();
		public void Activate();
		public bool TakeAction(IAction action);
	}
}
