﻿using Homework.ThirdParty;
using System.Collections.Generic;
using System.Linq;

namespace Homework.UnitTests
{
    class FakeAccountRepository : IAccountRepository
    {
        private List<Account> _accounts = new List<Account>();

        public bool Add(Account account)
        {
            try
            {
                _accounts.Add(account);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool Exists(int accountId)
        {
            return _accounts.Exists(account => account.Id == accountId);
        }

        public Account Get(int accountId)
        {
            return _accounts.FirstOrDefault(account => account.Id == accountId);
        }

        public IEnumerable<Account> GetAll()
        {
            return _accounts.ToList();
        }

        public bool Remove(int accountId)
        {
            int removedCount = _accounts.RemoveAll(account => account.Id == accountId);
            return 0 < removedCount;
        }
    }
}
