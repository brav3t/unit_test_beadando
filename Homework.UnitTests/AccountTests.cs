﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;

namespace Homework.UnitTests
{
    [TestFixture]
    class AccountTests
    {
        private int _id;
        private Account _account;
        private Mock<IAction> _mockAction;

        [SetUp]
        public void SetUp()
        {
            _id = 0;
            _account = new Account(_id);
        }

        [Test]
        public void ValidAccount_Create_Id()
        {
            Assert.That(_account.Id, Is.EqualTo(_id));
        }

        [Test]
        [Category("Registration")]
        public void ValidAccount_Create_IsNotRegistered()
        {
            Assert.That(_account.IsRegistered, Is.False);
        }

        [Test]
        [Category("Registration")]
        public void ValidAccount_Register_IsRegistered()
        {
            _account.Register();

            Assert.That(_account.IsRegistered, Is.True);
        }

        [Test]
        [Category("Validation")]
        public void ValidAccount_Create_IsNotConfirmed()
        {
            Assert.That(_account.IsConfirmed, Is.False);
        }

        [Test]
        [Category("Validation")]
        public void ValidAccount_Activate_IsConfirmed()
        {
            _account.Activate();

            Assert.That(_account.IsConfirmed, Is.True);
        }

        [Test]
        [Category("TakeAction")]
        public void ValidAccount_TakeAction_NotRegisteredNotConfirmed()
        {
            _mockAction = new Mock<IAction>();
            bool actionResult = false;
            _mockAction.Setup(action => action.Execute()).Returns(actionResult);

            Assert.That(() => _account.TakeAction(_mockAction.Object), Throws.TypeOf<InactiveUserException>());
        }

        [Test]
        [Category("TakeAction")]
        public void ValidAccount_TakeAction_NotConfirmed()
        {
            _mockAction = new Mock<IAction>();
            bool actionResult = false;
            _mockAction.Setup(action => action.Execute()).Returns(actionResult);

            _account.Register();

            Assert.That(() => _account.TakeAction(_mockAction.Object), Throws.TypeOf<InactiveUserException>());
        }

        [Test]
        [Category("TakeAction")]
        public void ValidAccount_TakeAction_NotRegistered()
        {
            _mockAction = new Mock<IAction>();
            bool actionResult = false;
            _mockAction.Setup(action => action.Execute()).Returns(actionResult);

            _account.Activate();

            Assert.That(() => _account.TakeAction(_mockAction.Object), Throws.TypeOf<InactiveUserException>());
        }

        [Test]
        [Category("TakeAction")]
        public void ValidAccount_TakeAction_UnsuccessfullAction()
        {
            _mockAction = new Mock<IAction>();
            bool actionResult = false;
            _mockAction.Setup(action => action.Execute()).Returns(actionResult);

            _account.Register();
            _account.Activate();
            bool takeActionResult = _account.TakeAction(_mockAction.Object);

            Assert.That(takeActionResult, Is.False);
            Assert.That(_account.ActionsSuccessfullyPerformed, Is.EqualTo(0));
        }

        [Test]
        [Category("TakeAction")]
        public void ValidAccount_TakeAction_SuccessfullAction()
        {
            _mockAction = new Mock<IAction>();
            bool actionResult = true;
            _mockAction.Setup(action => action.Execute()).Returns(actionResult);

            _account.Register();
            _account.Activate();
            bool takeActionResult = _account.TakeAction(_mockAction.Object);

            Assert.That(takeActionResult, Is.True);
            Assert.That(_account.ActionsSuccessfullyPerformed, Is.EqualTo(1));
        }

        [Test]
        [Category("TakeAction")]
        public void ValidAccount_TakeAction_MultipleSuccessfullAction()
        {
            _mockAction = new Mock<IAction>();
            bool actionResult = true;
            _mockAction.Setup(action => action.Execute()).Returns(actionResult);

            _account.Register();
            _account.Activate();
            int takeActionRunCount = 20;
            bool takeActionResult = false;
            for (int i = 0; i < takeActionRunCount; i++)
            {
                takeActionResult = _account.TakeAction(_mockAction.Object);
            }

            Assert.That(takeActionResult, Is.True);
            Assert.That(_account.ActionsSuccessfullyPerformed, Is.EqualTo(takeActionRunCount));
        }
    }
}
