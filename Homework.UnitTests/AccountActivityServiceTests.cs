﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Homework.UnitTests
{
    [TestFixture]
    class AccountActivityServiceTests
    {
        private FakeAccountRepository _fakeAccountRepository;
        private AccountActivityService _accountActivityService;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
        }

        [SetUp]
        public void SetUp()
        {
            _fakeAccountRepository = new FakeAccountRepository();
        }

        [Test]
        [Category("GetActivity")]
        public void AccountActivityService_GetActivity_NoAccountWithIdPresent()
        {
            int accountId = 0;

            _accountActivityService = new AccountActivityService(_fakeAccountRepository);

            Assert.That(() => _accountActivityService.GetActivity(accountId), Throws.TypeOf<AccountNotExistsException>());
        }
        private Account createAccountWithActionCount(int actionRunCount)
        {
            Account account = new Account(actionRunCount);
            account.Register();
            account.Activate();
            Mock<IAction> mockAction = new Mock<IAction>();
            mockAction.Setup(action => action.Execute()).Returns(true);
            for (int i = 0; i < actionRunCount; i++)
            {
                account.TakeAction(mockAction.Object);
            }
            return account;
        }

        [TestCase(0,  ActivityLevel.None)]
        [TestCase(19, ActivityLevel.Low)]
        [TestCase(39, ActivityLevel.Medium)]
        [TestCase(40, ActivityLevel.High)]
        [Category("GetActivity")]
        public void AccountActivityService_GetActivity_ActivityLevels(
            int actionCount,
            ActivityLevel expectedActivityLevel)
        {
            Account account = createAccountWithActionCount(actionCount);
            _fakeAccountRepository.Add(account);
            _accountActivityService = new AccountActivityService(_fakeAccountRepository);

            ActivityLevel activityLevel = _accountActivityService.GetActivity(actionCount);

            Assert.That(activityLevel, Is.EqualTo(expectedActivityLevel));
        }

        [Test]
        public void AccountActivityService_GetActivity_GetSentToRepo()
        {
            Mock<IAccountRepository> _mockAccountRepository = new Mock<IAccountRepository>();
            int actionCount = 0;
            Account account = createAccountWithActionCount(actionCount);
            _mockAccountRepository.Setup(provider => provider.Get(It.IsAny<int>())).Returns(account);
            _accountActivityService = new AccountActivityService(_mockAccountRepository.Object);

            _accountActivityService.GetActivity(actionCount);

            _mockAccountRepository.Verify(repository => repository.Get(It.IsAny<int>()), Times.Once);
        }
        
        [TestCase(0,  ActivityLevel.None,   0)]
        [TestCase(19, ActivityLevel.Low,    1)]
        [TestCase(39, ActivityLevel.Medium, 2)]
        [TestCase(40, ActivityLevel.High,   3)]
        [Category("GetAmountForActivity")]
        public void AccountActivityService_GetAmountForActivity_ActivityLevels(
            int actionCount,
            ActivityLevel activityLevel,
            int expectedAmount)
        {
            for (int i = 0; i < expectedAmount; i++)
            {
                Account acount = createAccountWithActionCount(actionCount);
                _fakeAccountRepository.Add(acount);
            }
            _accountActivityService = new AccountActivityService(_fakeAccountRepository);

            int amount = _accountActivityService.GetAmountForActivity(activityLevel);

            Assert.That(amount, Is.EqualTo(expectedAmount));
        }

        [Test]
        public void AccountActivityService_GetAmountForActivity_GetAllAndGetSentToRepo()
        {
            Mock<IAccountRepository> _mockAccountRepository = new Mock<IAccountRepository>();
            int actionCount = 0;
            Account account = createAccountWithActionCount(actionCount);
            _mockAccountRepository.Setup(repository => repository.Get(It.IsAny<int>())).Returns(account);
            List<Account> accounts = new List<Account>();
            accounts.Add(account);
            _mockAccountRepository.Setup(repository => repository.GetAll()).Returns(accounts);
            _accountActivityService = new AccountActivityService(_mockAccountRepository.Object);
            ActivityLevel activityLevel = ActivityLevel.None;

            _accountActivityService.GetAmountForActivity(activityLevel);

            _mockAccountRepository.Verify(repository => repository.GetAll(), Times.Once);
            _mockAccountRepository.Verify(repository => repository.Get(It.IsAny<int>()), Times.Once);
        }
    }
}
